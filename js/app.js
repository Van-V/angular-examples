angular.module('directoryApp', [])
    .controller('directoryController', function($scope) {
        $scope.list = [
            {name: "Mike", age: 40},
            {name: "john", age: 10},
            {name: "Jilla", age: 12},
            {name: "Martin", age: 45},
            {name: "Joshep", age: 33},
            {name: "Sarah", age: 14}
        ];

        $scope.addMembers = function() {
            $scope.list.push({name:$scope.name, age:$scope.age});
            $scope.name = '';
            $scope.age = 0;
        };
    });